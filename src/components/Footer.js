import React, {Component} from 'react';

class Footer extends Component{

    constructor(props){
        super(props);
    }

    render(){
        return (
            <div className="container">
                <div>
                    <a href="https://coreui.io">Free</a>
                    <span>&copy; Citations</span>
                </div>
                <div className="ml-auto">
                    <span>Powered by</span>
                    <a href=""> 2019 Moussa OUALIBOU.</a>

            </div>
            </div>
        )
    }

}

export default Footer;