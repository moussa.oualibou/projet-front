import React, {Component} from 'react';
import {Link} from 'react-router-dom';

class Citation extends Component{

    constructor(props){
        super(props);
    }

    componentDidMount(){
        console.log(this.props);
    }


    render(){
        return(
            <tr>
    
               <td> <button>{<Link to={`/citations/${this.props.data._id}`}>Detail</Link>}</button></td> 
               <td><img class="card-img-top" src={require(`./images/${this.props.data.image}`)} alt="" /></td>
                <td>{this.props.data.title}</td>
                <td>{this.props.data.content.substr(0, 25)}...</td>
                <td>{this.props.data.user_id.nom}</td>

                
                  {
                  /*  <td><button className="btn btn-danger" onClick={() => this.props.deletePost(this.props.data.id)>Supprimer</button> </td>*/}
                
            </tr>
        )
    }


}

export default Citation;