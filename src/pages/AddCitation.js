import React, {Component} from 'react';
import CitationService from '../services/citations.service';

class AddCitation extends Component{

    constructor(props){
        super(props);
        this.state = {
            title: '',
            content: '',
            image: '',
            success: false,
            msgSuccess: '',
        }  
    }

    //Change value instead of id state
    handleChange(e){
        //change state
        this.setState({
            [e.target.id]: e.target.value
        });
    }
    
    async submit(e){
        e.preventDefault();
        this.setState({success: false});
        let body = {
            title: this.state.title,
            content: this.state.content,
            image: this.state.image,
        }; 
        let response = await CitationService.create(body);
        if(response.ok){
            this.setState({
                success: true,
                msgSuccess: "Citation is created with successfull"
            })
        }
    }

    render(){
        return (
            <div className="container">
                <h1>Ajouter un Citation</h1>
                <form onSubmit={(e) => this.submit(e)}>
                    <div className="form-group">
                        <label>Titre</label>
                        <input type="text" className="form-control" required id="title" value={this.state.title} onChange={(e) => this.handleChange(e)}/>
                    </div>
                    <div className="form-group">
                        <label>Content</label>
                        <input type="text" className="form-control" required id="content" value={this.state.content} onChange={(e) => this.handleChange(e)}/>
                    </div>


                    <div className="form-group">
                        <label>Image</label>
                        <input type="text" className="form-control" required id="image" onChange={(e) => this.handleChange(e)}/>
                    </div>

                    <button type="submit" className="btn btn-primary">Ajouter</button>
                </form>

                {
                    this.state.success === true ? <p>{this.state.msgSuccess}</p> : null 
                }

            </div>
        )
    }

}

export default AddCitation;