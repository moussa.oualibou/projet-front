import React, {Component} from 'react';
import CitationService from '../services/citations.service';

class DetailsCitation extends Component{

    constructor(props){
        super(props);
        this.state = {
            citation: []
        }
    }

    async componentDidMount(){
        let id = this.props.match.params.id;
        let response = await CitationService.details(id);
        if(response.ok){
            let data = await response.json();
            this.setState({citation: data.citations});
            console.log(data.citations)
        }
    }


    render(){
        return(
         /*    <div className="container">

                <h1>{this.state.citation ? this.state.citation.title : null }</h1>
                <img src={this.state.citation ? this.state.citation.content : null} ></img>
                <p>{this.state.citation ? this.state.citation.image : null }</p>
                 <button className="btn btn-danger" >Supprimer</button>
                 <img class="card-img-top" src={uneCitation.image} alt="" />
            </div> */
            <div class="card">
            
            <div class="card-body">
                
                <h4 class="card-title">{this.state.citation.title}</h4>
                <h4 class="card-title">{this.state.citation.content}</h4>
                <p class="card-text">
                    <a  className="btn-success">Supprimer</a>
                    <a  className="btn-warning">Modifier</a>
                </p>
            </div>
        </div>
        )
        
        
        
        
        
        
        
        
        
          
        
        
        
        
        
    


    }
}

export default DetailsCitation;