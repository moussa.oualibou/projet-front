import React, {Component} from 'react';
import Citation from '../components/Citation';
import CitationsService from '../services/citations.service';

class Home extends Component{

    constructor(props){
        super(props);
        
        //base de données interne
        this.state = {
            citation: []
        }


    }
    
    async componentDidMount(){        
        //Récupération des citations depuis l'API
        let response = await CitationsService.list();
        if(response.ok){
            //La réponse est de type 200
            let data = await response.json();
            this.setState({citation: data.citations});
            console.log(data.citations)
        }
    }
/*

    async deleteCitation(id){
        let response = await CitationsService.delete(id);
        if(response.ok){
            let citations = this.state.citations;
            let index = citations.findIndex(post => post.id === id);
            citations.splice(index, 1);

            this.setState({citations: citations});
        }
    }*/

    render(){
        return(
            <div className="container">
                <table className="table">
                    <thead>
                        <tr>
                            
                            <th>#</th>
                            <th>photo</th>
                            <th>Titre</th>
                            <th>Contenu</th>
                            <th>Auteur</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            this.state.citation.length !== 0 ?
                                this.state.citation.map((item) => {
                                    return (
                                        <Citation data={item}/>
                                    )
                                })
                            : <p>Il n'y a aucun Citation.</p>
                        }
                    </tbody>
                </table>
            </div>
        )
    }
}

export default Home;