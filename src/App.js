import React, {Component} from 'react';
import {BrowserRouter, Route} from 'react-router-dom';
import Home from './pages/Home';
import DetailsCitation from './pages/DetailsCitation';
import Header from './components/Header';
import Footer from './components/Footer';
import AddCitation from './pages/AddCitation';

class App extends Component{

    render(){
        return (       
            <BrowserRouter>
                <Header />
                <Route path="/" exact component={Home} />
                <Route path="/citations/:id" exact component={DetailsCitation} />
                <Route path="/AddCitations" exact component={AddCitation} />
                <Footer />
            </BrowserRouter>
        )
    }
}

export default App;
